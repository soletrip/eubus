/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  NavigatorIOS,
} = React;

var AV = require('avoscloud-sdk');
AV.initialize('3FEo2vazWl3uhMaXaMIxcEJ1', 'n3S40ID1HOidexcvKTu1hKEY');
AV.Promise.setPromisesAPlusCompliant(true);

var Welcome = require('./ui/Welcome');
var MainScreen = require('./ui/MainScreen');
var OrderScreen = require('./ui/OrderScreen');
var MinorScreen = require('./ui/MinorScreen');
var DatePickerScreen = require('./ui/DatePickerScreen');
var Login = require('./components/Login');
var UserPortal = require('./components/user/UserPortal')

var Youbus = React.createClass({
  RouteMapper: function(route, navigationOperations, onComponentRef) {
    if (route.name === 'welcome') {
      return (
        <View style={styles.container}>
          <Welcome navigator={navigationOperations}/>
        </View>
      );
    } else if (route.name === 'main') {
      return (
        <View style={styles.container}>
            <MainScreen navigator={navigationOperations}/>
        </View>
      );
    } else if (route.name === 'order') {
      return (
        <View style={styles.container}>
            <OrderScreen navigator={navigationOperations} title={route.title}/>
        </View>
      );
    } else if (route.name === 'minor') {
      return (
        <View style={styles.container}>
          <MinorScreen navigator={navigationOperations} title={route.title}/>
        </View>
      );
    } else if (route.name === 'login') {
      return (
        <View style={styles.container}>
          <Login navigator={navigationOperations}/>
        </View>
      );
    }  else if (route.name === 'profile') {
      return (
        <View style={styles.container}>
          <UserPortal navigator={navigationOperations}/>
        </View>
      );
    } else if (route.name === 'datepicker') {
      return (
        <View style={styles.container}>
            <DatePickerScreen navigator={navigationOperations} title={route.title}
            selectedDate={route.selectedDate} onDateChange={route.onDateChange}/>
        </View>
      );
    };
  },

  render: function() {
    // var initialRoute = {
    //   component: Welcome,
    // };
    var initialRoute = {name: 'welcome'};
    return (
      <Navigator
        style={styles.container}
        initialRoute={initialRoute}
        configureScene={() => Navigator.SceneConfigs.FadeAndroid}
        renderScene={this.RouteMapper}
      />
      // <NavigatorIOS
      //   style={styles.container}
      //   // navigationBarHidden={true}
      //   initialRoute={initialRoute}/>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'transparent',
  },
});

AppRegistry.registerComponent('Youbus', () => Youbus);
