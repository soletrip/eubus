/**
 *	User portal page for eubus
 *
 */
'use strict';

var React = require('react-native');
var {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} = React;

var NaviBar = require('../../ui/NaviBar');
var Util = require('../../ui/Util');
var Footer = require('./UserFooter');

function getUserState() {
  return {
    profile: UserStore.getProfile()
  };
}

var UserPortal = React.createClass({
  _onPressBackButton: function() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  },

  _onChange: function() {
    this.setState(getUserState());
  },

  getInitialState: function() {
    return getUserState();
  },

  componentDidMount: function() {
    UserStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    UserStore.removeChangeListener(this._onChange)
  },

	render: function() {
	return (
      <View style={styles.container}>
        <NaviBar style={styles.header} navigator={this.props.navigator}/>

        <View style={styles.profileHeader}>
          <View style={styles.badge}>
            <Text style={styles.label}>头像</Text>
          </View>
          <View style={styles.button}>
          <Text style={styles.label}>未登录</Text>
          </View>
        </View>
        <View style={styles.profileMenus}>
          <View style={styles.profileMenu}>
            <Text style={styles.label, {fontSize: 21}}>注册</Text>
          </View>
          <View style={styles.profileMenu}>
            <Text style={styles.label, {fontSize: 21}}>客户服务</Text>
          </View>
          <View style={styles.profileMenu}>
            <Text style={styles.label, {fontSize: 21}}>帮助</Text>
          </View>
        </View>

        <Footer navigator={this.props.navigator}
          ver='V0.1'
          updateTips='已更新到最新版本.'
        />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    width: Util.size.width,
  },
  profileHeader: {
    height: Util.size.width*0.6,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#CCCCCC'
  },
  badge: {
    width: 96,
    height: 96,
    backgroundColor: '#FF6666',
    borderRadius: 48,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 25,
  },
  button: {
    backgroundColor: '#0099CC',
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
  },
  label: {
    fontSize: 18,
  },
  profileMenus: {
    flex: 1,
    flexDirection: 'column',
  },
  profileMenu: {
    flexDirection: 'row',
    padding: 30,
    paddingLeft: 30,
    alignItems: 'center',
  },
});

module.exports = UserPortal;
