'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} = React;

var UserFooter = React.createClass ({

  render: function() {
    return (
      <View style={styles.footer}>
        <Text style={styles.label}>{this.props.ver}</Text>
        <Text style={styles.label}>{this.props.updateTips}</Text>
      </View>
    );
  }
});

var styles = StyleSheet.create ({
  footer: {
    flexDirection: 'column',
    backgroundColor: '#666666',
    height: 72,
    alignItems: 'center',
  },
  label: {
    fontSize: 18,
    color: '#CC9966',
    margin: 5,
  }
});

module.exports = UserFooter;
