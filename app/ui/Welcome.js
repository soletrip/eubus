/**
 *	Welcome page for eubus
 *
 */
 'use strict';
var MENUS_DATA = [
  {title: '询车', icon: {thumbnail: require('image!home_menu03')}},
  {title: '询导', icon: {thumbnail: require('image!home_menu04')}},
  {title: '车兼导', icon: {thumbnail: require('image!home_menu01')}},
];

var React = require('react-native');
var {
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  NavigatorIOS,
  TouchableHighlight,
  TouchableOpacity,
  Platform,
} = React;

var MainScreen = require('./MainScreen');
var OrderScreen = require('./OrderScreen');
var MinorScreen = require('./MinorScreen');

var Welcome = React.createClass({
  onBus: function(event) {
    if (Platform.OS === 'ios1') {
      this.props.navigator.push({
        title: MENUS_DATA[0].title,
        component: MainScreen,
        passProps: {},
      });
    } else {
      this.props.navigator.push({
        title: MENUS_DATA[0].title,
        name: 'minor',
      });
    }
  },
  onGuider: function(event) {
    if (Platform.OS === 'ios1') {
      this.props.navigator.push({
        title: MENUS_DATA[1].title,
        component: MainScreen,
        passProps: {},
      });
    } else {
      this.props.navigator.push({
        title: MENUS_DATA[1].title,
        name: 'minor',
      });
    }
  },
  onBusGuider: function(event) {
    if (Platform.OS === 'ios1') {
      this.props.navigator.push({
        title: MENUS_DATA[2].title,
        component: MainScreen,
        passProps: {},
      });
    } else {
      this.props.navigator.push({
        title: MENUS_DATA[2].title,
        name: 'minor',
      });
    }
  },
  onOrder: function(event) {
    if (Platform.OS === 'ios1') {
      this.props.navigator.push({
        title: 'My Order',
        component: OrderScreen,
        passProps: {},
      });
    } else {
      this.props.navigator.push({
        title: 'My Order',
        name: 'profile',
      });
    }
  },
	render: function() {
    var menus = MENUS_DATA;
  	return (
        <View style={styles.container}>
          <Image
            style={styles.bgimg}
            source={require('image!bg')}>
          <Text style={styles.welcome}>
            有巴士, 游巴适
          </Text>
          <View style={styles.entries}>
            <TouchableOpacity onPress={this.onBus}>
            <View style={styles.entry}>
              <Image style={styles.thumbnail}
                source={menus[0].icon.thumbnail} />
              <Text style={styles.instructions}>
                {menus[0].title}
              </Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onGuider}>
            <View style={styles.entry}>
              <Image style={styles.thumbnail}
                source={menus[1].icon.thumbnail} />
              <Text style={styles.instructions}>
                {menus[1].title}
              </Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onBusGuider}>
            <View style={styles.entry}>
              <Image style={styles.thumbnail}
                source={menus[2].icon.thumbnail} />
              <Text style={styles.instructions}>
                {menus[2].title}
              </Text>
            </View>
            </TouchableOpacity>
          </View>
          <View style={styles.line}>
          </View>
          <TouchableOpacity onPress={this.onOrder}>
          <View style={styles.orderContainer}>
            <Image
              style={styles.thumbnailNormal}
              source={{uri: 'home_menu_order'}} />
            <View style={styles.entry}>
              <Text style={styles.myOrder}>
                我的行程
              </Text>
              <Text style={styles.tips}>
                查看所有已预订行程
              </Text>
            </View>
          </View>
          </TouchableOpacity>
          <View style={styles.line}>
          </View>
          </Image>
        </View>
      );
    }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  bgimg: {
    flex: 1,
    alignItems: 'center',
    resizeMode: Image.resizeMode.stretch,
  },
  welcome: {
    fontSize: 32,
    textAlign: 'center',
    margin: 60,
    marginTop: 80,
    color: '#EFEFDF',
    backgroundColor: 'transparent',
  },
  entries: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  entry: {
    flexDirection: 'column',
    justifyContent: 'center',
    margin: 16,
  },
  instructions: {
    textAlign: 'center',
    color: '#DFDFDF',
    margin: 10,
    fontSize: 18,
  },
  myOrder: {
    textAlign: 'left',
    color: '#DFDFDF',
    margin: 10,
    fontSize: 18,
  },
  tips: {
    textAlign: 'center',
    color: '#CECECE',
    fontSize: 15,
  },
  thumbnail: {
    alignSelf: 'center',
    width: 64,
    height: 64,
    borderRadius: 33,
  },
  thumbnailNormal: {
    alignSelf: 'center',
    width: 64,
    height: 64,
  },
  line: {
    width: 480,
    flexDirection: 'row',
    borderColor: 'grey',
    borderWidth: 1,
    opacity: 0.6,
    marginTop: 5,
    marginBottom: 5,
  },
  orderContainer: {
    flexDirection: 'row',
    margin: 5,
    backgroundColor: 'transparent',
  },
});

module.exports = Welcome;
