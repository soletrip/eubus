'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} = React;

var NaviBar = React.createClass ({

  _onPressBackButton: function() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  },

  render: function() {
    // <View><Text style={styles.toolbarBack}>{'<'}</Text></View>
    return (
      <View style={styles.toolbar}>
        <TouchableOpacity onPress={this._onPressBackButton}>
        <Image style={styles.back} source={require('image!top_back_black')}/>

        </TouchableOpacity>
        <Text style={styles.toolbarTitle}>{this.props.title}</Text>
        <Text style={styles.toolbarButton}>Like</Text>
      </View>
    );
  }
});

var styles = StyleSheet.create ({
  back: {
    height: 20,
    width: 20,
    marginLeft: 12,
    opacity: 0.6,
  },
  toolbar:{
    backgroundColor:'#0DA3DD',
    paddingTop:30,
    paddingBottom:10,
    flexDirection:'row',    //Step 1
  },
  toolbarButton:{
    width: 50,            //Step 2
    color:'#fff',
    textAlign:'center'
  },
  toolbarBack: {
    width: 50,            //Step 2
    color:'#fff',
    textAlign:'center',
    fontSize: 18,
  },
  toolbarTitle:{
    color:'#fff',
    textAlign:'center',
    fontWeight:'bold',
    flex:1,                //Step 3
    fontSize: 18,
  },
});

module.exports = NaviBar;
