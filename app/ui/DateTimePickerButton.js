
'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} = React;

var DateTimePickerButton = React.createClass ({
  getDefaultProps: function() {
    return {
      onDateChange () {}
    }
  },
  getInitialState: function() {
    return {
      selected: false,
      date: null,
    };
  },
  componentWillMount: function() {
    if (this.props.date === null
    || this.props.date === undefined) {
      this.state.date = new Date();
    } else {
      this.state.date = this.props.date;
    }
  },
  onDateChange: function(date) {
    console.log(date);
    this.setState({date: date, selected: true});
    this.props.onDateChange(date);
  },
  onDatePicker: function() {
    this.props.navigator.push({
      title: '选择' + this.props.text,
      name: 'datepicker',
      selectedDate: this.state.date,
      onDateChange: this.onDateChange,
    });
  },

  _onPressBackButton: function() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  },

  formatDate: function(date) {
    console.log('formatDate iso: ' + date.toISOString());
    console.log('formatDate json: ' + date.toLocaleDateString());
    // return date.toISOString().replace(/(T.*)/g, '');
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
  },

  render: function() {
    if (this.state.selected) {
      return (
        <TouchableOpacity onPress={this.onDatePicker} style={styles.button}>
        <View style={styles.button}>
          <Text style={styles.text}>{this.props.text}</Text>
            <Text style={styles.value}>{this.formatDate(this.state.date)}</Text>
        </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity onPress={this.onDatePicker} style={styles.button}>
        <View style={styles.button}>
          <Text style={styles.text}>{this.props.text}</Text>
            <Text style={styles.value}>选择日期</Text>
        </View>
        </TouchableOpacity>
      );
    }
  }
});

var styles = StyleSheet.create ({
  button: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    paddingBottom: 6,
  },
  value: {
    color: '#B5B5B5',
    fontSize: 15,
  },
});

module.exports = DateTimePickerButton;
