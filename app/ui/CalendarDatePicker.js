'use strict'

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
} = React;

var CalendarPicker = require('react-native-calendar-picker');

var CalendarDatePicker = React.createClass({
  getDefaultProps: function() {
    console.log('getDefaultProps called ...');
  },
  getInitialState: function() {
    console.log('getInitialState called ...');
    return {
      date: new Date(),
    };
  },
  componentWillMount: function() {
    console.log('componentWillMount called ...');
  },
  componentDidMount: function(oldProps, oldState) {
    if (oldProps === undefined || oldState === undefined) {
      console.log('componentDidMount called ...');
    } else {
      console.log('componentDidMount called ...:' + oldProps.toString() + ' ' + oldState.toString());
    }
  },
  componentWillReceiveProps: function(newProps) {
    console.log('componentWillReceiveProps called ...:' + newProps.toString());
  },
  shouldComponentUpdate: function(newProps, newState) {
    console.log('shouldComponentUpdate called ...:' + newProps.toString() + ' ' + newState.toString());
    return true;
  },
  componentWillUpdate: function(newProps, newState) {
    console.log('componentWillUpdate called ...:' + newProps.toString() + ' ' + newState.toString());
  },
  componentWillUnmount: function() {
    console.log('componentWillUnmount called ...');
  },

  onDateChange: function(date) {
    console.log('onDateChange called ...:' + date);
    this.setState({ date: date });
    console.log('onDateChange date: ...' + this.state.date);
  },

  render: function() {
    console.log('render called ...');
    return (
      <View style={styles.container}>

        <CalendarPicker
          selectedDate={this.state.date}
          onDateChange={this.onDateChange} />

        <Text style={styles.selectedDate}>Date: {this.state.date.toString()}:{ this.state.date.getMonth() }-{this.state.date.getDate() } </Text>
      </View>

    );
  }
});
var styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  selectedDate: {
    color: '#B5B5B5',
    fontSize: 15,
  },
});

module.exports = CalendarDatePicker;
