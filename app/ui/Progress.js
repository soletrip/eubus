'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} = React;

var Progress = React.createClass ({

  _onPressBackButton: function() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  },

  render: function() {
    return (
      <View style={styles.progress_container}>
      <View style={styles.progress}>
        <View style={styles.progress_entry}>
          <Image
            source={require('image!progress_bar_img03')}/>
          <View style={styles.progress_item}>
            <Text style={styles.progress_tip}>
              填写行程
            </Text>
            <View style={styles.progress_line}/>
          </View>
        </View>

        <View style={styles.progress_entry}>
          <Image
            source={require('image!progress_bar_img02')}/>
          <View style={styles.progress_item}>
            <Text style={styles.progress_tip}>
              选车
            </Text>
            <View style={styles.progress_line}/>
          </View>
        </View>

        <View style={styles.progress_entry}>
          <Image
            source={require('image!progress_bar_img')}/>
          <View style={styles.progress_item}>
            <Text style={styles.progress_tip}>
              提交询价单
            </Text>
            <View style={styles.progress_line}/>
          </View>
        </View>
      </View>
      </View>
    );
  }
});

var styles = StyleSheet.create ({
  progress_container: {
    height: 36,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  progress: {
    flexDirection: 'row',
  },
  progress_entry: {
    flex: 1,
    flexDirection: 'row',
  },
  progress_item: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  progress_tip: {
    alignSelf: 'auto',
    color: '#DFDFDF',
  },
  progress_line: {
    width: 80,
    height: 1,
    marginTop: 3,
    backgroundColor: '#AEAEAE',
  },
});

module.exports = Progress;
