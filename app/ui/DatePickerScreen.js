/**
 *	Main page for eubus
 *
 */
'use strict';

var React = require('react-native');
var {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} = React;

var Util = require('./Util')
var NaviBar = require('./NaviBar')
var Footer = require('./Footer');
var DatePicker = require('./DatePicker')

var DatePickerScreen = React.createClass({
  propTypes: {
    selectedDate: React.PropTypes.instanceOf(Date).isRequired,
    onDateChange: React.PropTypes.func
  },
  getDefaultProps: function() {
    return {
      onDateChange () {}
    }
  },
  getInitialState: function() {
    return {
      date: this.props.selectedDate,
    };
  },

  onDateChange: function(date, commit) {
    console.log("picker: " + date);
    this.setState({date: date,});
    this.props.onDateChange(date);
    if (commit) {
      this._onPressBackButton();
    }
  },

  _onPressBackButton: function() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  },
	render: function() {
	return (
    <View style={styles.container}>
      <NaviBar style={styles.header} navigator={this.props.navigator} title={this.props.title}/>
      <DatePicker navigator={this.props.navigator} selectedDate={this.state.date} onDateChange={this.onDateChange}/>
      <Footer style={styles.footer} navigator={this.props.navigator}/>
    </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    width: Util.size.width,
  },
  footer: {
    borderWidth: 3,
    borderColor: 'yellow',
  },
});

module.exports = DatePickerScreen;
