'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  TextInput,
  View,
  Util,
} = React;

var Util = require('./Util');
var Dates = require('./Dates');
var MyButton = require('./MyButton');

var BasicInfo = React.createClass ({

  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.line}>
          <TextInput style={styles.inputEditor}
            ref='textinput'
            placeholder='你从哪个城市出发?'
            onFocus={() => {this.refs.textinput.focus()}}>
          </TextInput>
        </View>
        <View style={styles.line}>
          <TextInput style={styles.inputEditor}
            ref='textinput2'
            placeholder='你在哪个城市结束旅行?'
            onFocus={() => {this.refs.textinput2.focus()}}>
          </TextInput>
        </View>
        <Dates navigator={this.props.navigator}/>
        <MyButton navigator={this.props.navigator}/>
        <Text style={styles.tips}>下单后, 我们会尽快为您的需求提供报价</Text>
      </View>
    );
  }
});

var styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F9F7ED',
    paddingLeft: 28,
    paddingRight: 28,
    paddingTop: 21,
  },
  line: {
    flexDirection:'row',
    alignItems:'center',
    justifyContent: 'center',
    marginBottom: 21,
  },
  inputEditor: {
    flex: 1,
    height: 43,
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#48BBEC',
    borderRadius: 4,
    paddingLeft: 8,
  },
  dates: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#48BBEC',
    borderRadius: 4,
    marginBottom: 41,
  },
  tips: {
    flexDirection:'row',
    alignSelf: 'center',
    marginBottom: 10,
  },
});

module.exports = BasicInfo;
