
'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} = React;

var DateTimePickerButton = require('./DateTimePickerButton');

var Dates = React.createClass ({

  render: function() {
    return (
      <View style={styles.container}>
        <DateTimePickerButton navigator={this.props.navigator} text='开始日期' type='start'/>
        <View style={styles.divideContainer}></View>
        <DateTimePickerButton navigator={this.props.navigator} text='结束日期' type='end'/>
      </View>
    );
  }
});

var styles = StyleSheet.create ({
  container: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#B5B5B5',
    borderRadius: 2,
    paddingTop: 10,
    paddingBottom: 10,
  },
  divideContainer: {
    flexDirection: 'column',
    backgroundColor: '#B5B5B5',
    width: 1,
  },
});

module.exports = Dates;
