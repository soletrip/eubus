'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
} = React;

var CalendarPicker = require('../packages/react-native-calendar-picker');
// var CalendarPicker = require('react-native-calendar-picker');
var DatePicker = React.createClass ({
  propTypes: {
    selectedDate: React.PropTypes.instanceOf(Date).isRequired,
    onDateChange: React.PropTypes.func
  },
  getDefaultProps() {
    return {
      onDateChange () {}
    }
  },
  getInitialState() {
    return {
      date: this.props.selectedDate,
    };
  },

  onDateChange(date, commit) {
    console.log("picker inner:" + date);
    this.setState({date: date,});
    this.props.onDateChange(date, commit);
  },

  render: function() {
    return (
      <View style={styles.container}>
        <CalendarPicker
          selectedDate={this.state.date}
          onDateChange={this.onDateChange}/>

        <Text style={styles.selectedDate}>Date:  { this.state.date.toString() } </Text>
      </View>
    );
  }
});

var styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  selectedDate: {
    color: '#B5B5B5',
    fontSize: 15,
  },
});

module.exports = DatePicker;
