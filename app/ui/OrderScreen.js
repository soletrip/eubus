/**
 *	Main page for eubus
 *
 */
 'use strict';

var React = require('react-native');
var {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} = React;

var Util = require('./Util')
var NaviBar = require('./NaviBar')
var Footer = require('./Footer');
var CalendarDatePicker = require('./CalendarDatePicker');

var OrderScreen = React.createClass({
  _onPressBackButton: function() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  },
	render: function() {
	return (
    <View style={styles.container}>
    <Image style={styles.bg}
      source={require('image!bg')}>
      <NaviBar style={styles.header} navigator={this.props.navigator} title={this.props.title}/>
      <CalendarDatePicker/>
      <Footer style={styles.footer} navigator={this.props.navigator}/>
    </Image>
    </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  bg: {
    width: Util.size.width,
    height: Util.size.height,
    alignSelf: 'center',
    opacity: 1,
  },
  header: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    width: Util.size.width,
  },
  footer: {
    borderWidth: 3,
    borderColor: 'yellow',
  },
});

module.exports = OrderScreen;
