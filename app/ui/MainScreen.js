/**
 *	Main page for eubus
 *
 */
 'use strict';

var React = require('react-native');
var {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} = React;

var BasicInfo = require('./BasicInfo')

var MainScreen = React.createClass({
  _onPressBackButton: function() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  },
	render: function() {
	return (
      <View style={styles.container}>
        <Image style={styles.bg}
          source={require('image!bg')}>
        <View style={styles.toolbar}>
          <TouchableOpacity onPress={this._onPressBackButton}>
          <View><Text style={styles.toolbarButton}>Add</Text></View>
          </TouchableOpacity>
          <Text style={styles.toolbarTitle}>This is the title</Text>
          <Text style={styles.toolbarButton}>Like</Text>
        </View>
        <View style={styles.progress_container}>
        <View style={styles.progress}>
          <View style={styles.progress_entry}>
            <Image
              source={require('image!progress_bar_img03')}/>
            <View style={styles.progress_item}>
              <Text style={styles.progress_tip}>
                填写行程
              </Text>
              <View style={styles.progress_line}/>
            </View>
          </View>

          <View style={styles.progress_entry}>
            <Image
              source={require('image!progress_bar_img02')}/>
            <View style={styles.progress_item}>
              <Text style={styles.progress_tip}>
                选车
              </Text>
              <View style={styles.progress_line}/>
            </View>
          </View>

          <View style={styles.progress_entry}>
            <Image
              source={require('image!progress_bar_img')}/>
            <View style={styles.progress_item}>
              <Text style={styles.progress_tip}>
                提交询价单
              </Text>
              <View style={styles.progress_line}/>
            </View>
          </View>
        </View>
        </View>

        <View style={styles.body_container}>
          <BasicInfo/>
        </View>
        <View style={styles.footer}>
        </View>
        </Image>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  bg: {
    flex: 1,
    alignSelf: 'center',
    resizeMode: 'cover',
    opacity: 0.8,
  },
  toolbar:{
      backgroundColor:'#81c04d',
      paddingTop:30,
      paddingBottom:10,
      flexDirection:'row'    //Step 1
  },
  toolbarButton:{
      width: 50,            //Step 2
      color:'#fff',
      textAlign:'center'
  },
  toolbarTitle:{
      color:'#fff',
      textAlign:'center',
      fontWeight:'bold',
      flex:1                //Step 3
  },
  progress_container: {
    height: 36,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  progress: {
    flexDirection: 'row',
  },
  progress_entry: {
    flex: 1,
    flexDirection: 'row',
  },
  progress_item: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  progress_tip: {
    alignSelf: 'auto',
  },
  progress_line: {
    width: 80,
    height: 1,
    marginTop: 3,
    backgroundColor: '#AEAEAE',
  },
  body_container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#DEDEDE',
  },
  footer: {
    height: 72,
    backgroundColor: '#AEAEAE',
  },
});

module.exports = MainScreen;
