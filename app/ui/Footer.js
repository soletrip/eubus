'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} = React;

var Footer = React.createClass ({

  render: function() {
    return (
      <View style={styles.footer}>

      </View>
    );
  }
});

var styles = StyleSheet.create ({
  footer: {
    flexDirection: 'row',
    backgroundColor: '#0DA3DD',
    height: 72,
  },
});

module.exports = Footer;
