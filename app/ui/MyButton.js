
'use strict';

var React = require('react-native');

var {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} = React;

var MyButton = React.createClass ({

  onButtonPressed: function() {
    this.props.navigator.push({
      title: 'Confirmed',
      name: 'order',
    });
  },
  render: function() {
    return (
      <TouchableOpacity onPress={this.onButtonPressed}>
      <View style={[styles.container, {backgroundColor: '#CECECE'}]}>
        <Text>去选车</Text>
      </View>
      </TouchableOpacity>
    );
  }
});

var styles = StyleSheet.create ({
  container: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#CECECE',
    borderRadius: 2,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 21,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

module.exports = MyButton;
