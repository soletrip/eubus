/**
 *	UserStore
 *
 */
'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var UserConstants = require('../constants/UserConstants');
var EventEmitter = require('eventemitter2').EventEmitter2;
var assign = require('object-assign');

var AV = require('avoscloud-sdk');
var UserProfile = AV.Object.extend('UserProfile');
var userProfile = null;

var CHANGE_EVENT = 'change';
var LOGIN_EVENT = 'login';
var LOGOUT_EVENT = 'logout';

var _currentUser = null

/**
 * Login a user
 */
function login(username, password) {

  var user = new AV.User();
  user.set("username", username);
  user.set("password", password);
  user.logIn().then((user) => {
    console.log('User logged in:', user);
    // maybe it's good to navigator to somewhere ?
    _currentUser = user;

    var q = new AV.Query(UserProfile);
    q.equalTo('userId', current.getObjectId());
    q.find().then(
      (profile) => {
        userProfile = profile;
        UserStore.emitChange();
      }
    ).catch(
      (error) => {
        console.log("Login with query Error: ", error);
        _currentUser = null;
        userProfile = null;
        UserStore.emitChange();
      }
    );
  }).catch(function(error) {
    console.log("Login Error: ", error);
    _currentUser = null;
    userProfile = null;
    UserStore.emitChange();
  });
}

/**
 * Logout a user
 */
function logout() {
  var user = _currentUser;
  console.log('User logged out:', user);
  _currentUser = null;
  userProfile = null;
  user.logout();
  UserStore.emitChange();
}

/**
 * Signup a user
 */
function signup(username, password) {
  var user = new AV.User();
  user.set("username", username);
  user.set("password", password);
  user.signUp().then((user) => {
    console.log('User signed up:', user);
    _currentUser = user;
    userProfile = new UserProfile();
    userProfile.name = "我是小小白";
    userProfile.avatar = "";
    userProfile.userId = user.getObjectId();
    userProfile.save().then(()=>{UserStore.emitChange()});
  }).catch(function(error) {
    console.log("Signup Error: ", error);
    if (userProfile != null) {
      userProfile.destroy();
      userProfile = null;
    }
    _currentUser = null;
    UserStore.emitChange();
  });
}

var UserStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  /**
   * @param {function} callback
   */
  addChangeListener: function(cb) {
    this.on(CHANGE_EVENT, cb);
  },
  /**
   * @param {function} callback
   */
  removeChangeListener: function(cb) {
    this.removeListener(CHANGE_EVENT, cb);
  },
  getProfile: function() {
    return userProfile;
  },
});

AppDispatcher.register(function(action) {
  switch (action.actionType) {
    case UserConstants.USER_LOGIN:
      var username = action.username;
      var password = action.password;
      login(username, password);
      break;
    case UserConstants.USER_LOGOUT:
      logout();
      break;
    case UserConstants.USER_CREATE:
      var username = action.username;
      var password = action.password;
      signup(username, password);
      break;
    default:
      break;
  }
});
