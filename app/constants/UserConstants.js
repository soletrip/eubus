/**
 *	UserConstants
 *
 */
'use strict';

var keyMirror = require('keymirror');

module.exports = keymirror({
  USER_CREATE: null,
  USER_LOGIN: null,
  USER_LOGOUT: null,
  USER_UPDATE: null,
});
