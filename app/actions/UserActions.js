/**
 *	UserActions
 *
 */
'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var UserConstants = require('../constants/UserConstants')

var UserActions = {
  /**
   * @param {string} username
   * @param {string} password
   */
  create: function(username, password) {
    AppDispatcher.dispatch({
      actionType: UserConstants.USER_CREATE,
      username: username,
      password: password
    });
  },

  /**
   * @param {string} username
   * @param {string} password
   */
  login: function(username, password) {
    AppDispatcher.dispatch({
      actionType: UserConstants.USER_LOGIN,
      username: username,
      password: password
    });
  },

  /**
   * logout current user
   */
  logout: function() {
    AppDispatcher.dispatch({
      actionType: UserConstants.USER_LOGOUT
    });
  },

  /**
   * @param {string}
   */
  update: function(text) {
    AppDispatcher.dispatch({
      actionType: UserConstants.USER_UPDATE,
      text: text
    });
  },
};


module.exports = UserActions;
